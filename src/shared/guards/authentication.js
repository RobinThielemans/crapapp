import { authenticationToken, apiUrls } from "../../config";
import { hashPassword } from "../services/password";

let sessionStorage = window.sessionStorage;

 function createToken(token) {
    sessionStorage.setItem(authenticationToken, token)
}

export function signout() {
    sessionStorage.removeItem(authenticationToken);
}

export function authenticate(user) {

    user = JSON.stringify(user);

    return fetch(apiUrls.authToken, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: user
    }).then((response) => {
        if (response.status === 200) {
            return response.text().then((text) => {
                let token = JSON.parse(text);
                createToken(token.authtoken);
            });
        }
    });
}
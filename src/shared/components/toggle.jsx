import React from "react";

const Toggle = ({ handler, meta }) => {
    return (
        <tr>
            <td>{meta.bugname}</td>
            <td>
                <div className="switch">
                    <label>
                        <input type="checkbox" id={meta.id} {...handler("checkbox")} />
                        <span className="lever"></span>
                    </label>
                </div>
            </td>
        </tr>
    );
}

export default Toggle;
import { apiUrls } from "../../config";

export function passwordToken() {
    const url = window.location.href;

    let token = url.substring(url.indexOf("=") + 1, url.length);

    return fetch(`${apiUrls.passwordToken}/${token}`);
}